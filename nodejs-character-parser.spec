%{?nodejs_find_provides_and_requires}
%global enable_tests 1
Name:                nodejs-character-parser
Version:             1.2.0
Release:             1
Summary:             Parse JavaScript one character at a time to look for snippets in templates
License:             MIT
URL:                 https://github.com/ForbesLindesay/character-parser
Source0:             https://github.com/ForbesLindesay/character-parser/archive/%{version}.tar.gz
BuildArch:           noarch
ExclusiveArch:       %{nodejs_arches} noarch
BuildRequires:       nodejs-packaging

%if 0%{?enable_tests}
BuildRequires:       mocha npm(better-assert)
%endif

%description
Parse JavaScript one character at a time to look for snippets in templates.
This is not a validator, it's just designed to allow you to have sections
of JavaScript delimited by brackets robustly.

%prep
%autosetup -n character-parser-%{version}
for i in LICENSE README.md; do
    sed -i -e 's/\r$//' "${i}"
done

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/character-parser
cp -pr package.json index.js \
    %{buildroot}%{nodejs_sitelib}/character-parser
%nodejs_symlink_deps
%if 0%{?enable_tests}

%check
%nodejs_symlink_deps --check
/usr/bin/mocha -R spec
%endif

%files
%doc LICENSE README.md
%{nodejs_sitelib}/character-parser

%changelog
* Fri Aug 21 2020 leiju <leiju4@huawei.com> - 1.2.0-1
- Package init
